#  QA Automação - Curso FC

Exemplo aplicado no tutorial de automação com a framework [Webdriver IO](https://webdriver.io), aplicando fundamentos sobre como configurar a framework, iniciar uma automação e fazer a modelagem de objetos.

### Pré requisitos

Baixar e instalar o NodeJS na máquina, o download pode ser realizado através do link: https://nodejs.org/en/download/

Após instalação do NodeJS, abrir o PowerShell ou outro terminal de preferência e executar o comando:
```
npm install -g yarn
```
Este irá instalar o Yarn como uma dependencia global na máquina.

### Instalação

clone o repositório na pasta de projetos e acesse o diretório pelo terminal

Dentro do diretório webmotors execute o comando abaixo para instalar o projeto:
```
yarn install
```

### Executando os testes

Após a instalação execute o comando abaixo para executar o script de teste:
```
yarn test
```