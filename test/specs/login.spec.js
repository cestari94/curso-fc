const login = require('../pageObjects/login.po');
const tempo = require('../pageObjects/tempo.po');

xdescribe('Preencher dados corretamente e logar', () => {
    it('Dado que estou na tela de login do FC Team', () => {
        browser.url('https://fcteam.fcamara.com.br/#/login');
        $('.splash-title').waitForDisplayed(10000, true);
    });
    it('Quando preencher o campo de e-mail', () => {
        login.campoEmail.addValue('eteste@email.com');
    });
    it('E o campo de senha', () => {
        login.campoSenha.addValue('fc123');
    });
    it('E clicar em logar', () => {
        login.botaoLogar.click();
    });
    it('Então o usuário deve ser direcionado para a tela de Meu Tempo', () => {
        tempo.mesAtual.waitForDisplayed(5000);
    });
});

xdescribe('Preencher dados corretamente e logar - Simplificado', () => {
    it('Dado que estou na tela de login do FC Team', () => {
        browser.url('https://fcteam.fcamara.com.br/#/login');
        $('.splash-title').waitForDisplayed(10000, true);
    });
    it('Quando realizar o login preenchendo e-mail e senha', () => {
        login.realizaLogin('eteste@email.com', 'fc123');
    });
    it('Então o usuário deve ser direcionado para a tela de Meu Tempo', () => {
        tempo.mesAtual.waitForDisplayed(5000);
    });
});

describe('Preencher apenas o e-mail e tentar logar', () => {
    it('Dado que estou na tela de login do FC Team', () => {
        browser.url('https://fcteam.fcamara.com.br/#/login');
        $('.splash-title').waitForDisplayed(10000, true);
    });
    it('Quando realizar o login preenchendo apenas o e-mail', () => {
        login.realizaLogin('eteste@email.com', null);
    });
    it('Então deve ser exibido um alerta informando que o preenchimento é necessário', () => {
        login.alertaSenha.isDisplayed();
    });
});
