// Declara a classe do POM
class Login {
    get campoEmail() { return $('input#username'); }
    get campoSenha() { return $('input#password'); }
    get botaoLogar() { return $('button[type="submit"]'); }
    get linkEsqueciSenha() { return $('div.forgot-password > a'); }
    get alertaSenha() { return $('input#password.ng-empty'); }

    // Funcao que preenche o campo de e-mail e senha e clica no botão para logar
    realizaLogin(email, senha) {
        if(email) {
            this.campoEmail.addValue(email);
        }
        if(senha) {
            this.campoSenha.addValue(senha);
        }
        this.botaoLogar.click();
    }
    
}

// Exporta a classe do POM para utilizar em outros arquivos
module.exports = new Login();